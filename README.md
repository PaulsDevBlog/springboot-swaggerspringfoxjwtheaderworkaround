# SpringBoot-SwaggerSpringFoxJWTHeaderWorkaround

This repo contains the SpringBoot project discussed within the respective PaulsDevBlog.com article. 
https://www.paulsdevblog.com/springboot-swagger-springfox-work-around-for-jwt-authentication-bearer-header/ 

For other articles and code, please visit:
https://www.paulsdevblog.com