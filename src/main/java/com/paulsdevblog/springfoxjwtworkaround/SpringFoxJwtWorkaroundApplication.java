package com.paulsdevblog.springfoxjwtworkaround;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Main entry point for application
 * 
 * @author PaulsDevBlog.com
 */
@SpringBootApplication
@ComponentScan({    
                "com.paulsdevblog",
                "com.paulsdevblog.springfoxjwtworkaround.config",
                "com.paulsdevblog.springfoxjwtworkaround.domain",
                "com.paulsdevblog.springfoxjwtworkaround.domain.model",
                "com.paulsdevblog.springfoxjwtworkaround.domain.service",
                "com.paulsdevblog.springfoxjwtworkaround.utility"
})
public class SpringFoxJwtWorkaroundApplication {
    
    public static final Logger logger = LoggerFactory.getLogger(SpringFoxJwtWorkaroundApplication.class );

    public static void main(String[] args) {
        
        SpringApplication.run(SpringFoxJwtWorkaroundApplication.class, args);
        
        logger.info("--Application Started--");
        logger.info("This is our SpringFox Work-around Example Micro-Service!");
    }
}
