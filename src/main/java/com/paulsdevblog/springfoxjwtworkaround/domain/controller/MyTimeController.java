
package com.paulsdevblog.springfoxjwtworkaround.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springfoxjwtworkaround.domain.service.MyTimeService;
import com.paulsdevblog.springfoxjwtworkaround.domain.model.MyTime;

/**
 * Simple REST controller example that uses MyTimeService
 *
 * @author paulsdevblog.com
 */

@RestController
@RequestMapping("/time")
public class MyTimeController {
    
    private Logger logger = LoggerFactory.getLogger(MyTimeController.class );
    
    @Autowired
    private MyTimeService myTimeService;
    
    
    @ApiOperation(value = "Get Current Host Date-Time")
    @ApiResponses(value = {
                        @ApiResponse( code = 200, message = "Successful", response = MyTime.class  ),
                        @ApiResponse( code = 400, message = "Bad request" ),
                        @ApiResponse( code = 401, message = "Unauthorized" ),
                        @ApiResponse( code = 403, message = "Forbidden" ),
                        @ApiResponse( code = 404, message = "Not Found" ),
                        @ApiResponse( code = 406, message = "Unacceptable or invalid fields" )
                        }
    )
    @RequestMapping(
        value={"/current" }, 
        method = RequestMethod.GET,
        produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<MyTime> currentTime(){
        
        MyTime currentMyTime = myTimeService.getMyTime();
        
        return new ResponseEntity<MyTime>( currentMyTime, HttpStatus.OK );
        
    }
    
}
