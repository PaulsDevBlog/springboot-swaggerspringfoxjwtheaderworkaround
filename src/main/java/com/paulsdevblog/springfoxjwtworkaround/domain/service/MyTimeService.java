
package com.paulsdevblog.springfoxjwtworkaround.domain.service;

import com.paulsdevblog.springfoxjwtworkaround.domain.model.MyTime;

/**
 * Simple service interface to MyTime
 *
 * @author paulsdevblog.com
 */
public interface MyTimeService {

    MyTime getMyTime();
    
}
