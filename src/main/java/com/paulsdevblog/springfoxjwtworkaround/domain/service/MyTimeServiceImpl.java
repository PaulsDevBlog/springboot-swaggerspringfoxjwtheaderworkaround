
package com.paulsdevblog.springfoxjwtworkaround.domain.service;

import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paulsdevblog.springfoxjwtworkaround.domain.model.MyTime;
import com.paulsdevblog.springfoxjwtworkaround.utility.DateTimeUtil;

/**
 * Simple service layer example for MyTime
 *
 * @author paulsdevblog.com
 */
@Service
public class MyTimeServiceImpl implements MyTimeService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger( MyTimeServiceImpl.class );
    
    @Override
    public MyTime getMyTime() {
        
        try {
        
            LOGGER.info("getMyTime called");

            DateTimeUtil dateTimeUtil = new DateTimeUtil();

            MyTime myTime = new MyTime( dateTimeUtil.now(), dateTimeUtil.nowISO8601(), dateTimeUtil.nowUnixEpoch() );
            
            return myTime;
            
        } catch (Exception ex){
            
            LOGGER.error("getMyTime Error: " + String.valueOf(ex.getMessage()));
            
            return null;
            
        }
        
    }
    
}
