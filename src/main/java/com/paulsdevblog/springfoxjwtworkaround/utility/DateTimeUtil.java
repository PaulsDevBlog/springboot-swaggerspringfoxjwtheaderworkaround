
package com.paulsdevblog.springfoxjwtworkaround.utility;

import java.time.format.DateTimeFormatter;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple utility methods to provide current date time
 *
 * @author paulsdevblog.com
 */
public class DateTimeUtil {
 
    private static final long serialVersionUID = 1L;

    /**
     * Get current Java Date
     * 
     * @return Java Date
     */
    public Date now() {
        return new Date();
    }
    
    /**
     * Get current ISO 8601 date-time string
     * 
     * @return ISO 8601 string
     */
    public String nowISO8601(){
        
        String now_utc_dt = "";
        
        try {
        
            Instant instant = Instant.now();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

            now_utc_dt = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).format(formatter);

            return now_utc_dt;
        } catch ( Exception ex ){
            return null;
        }
    }
    
    /**
     * Get current Unix Epoch Second
     * 
     * @return Epoch second
     */
    public long nowUnixEpoch(){
        
        long unix_epoch_ts = 0;
        
        try {
        
            Instant instant = Instant.now();

            unix_epoch_ts = instant.getEpochSecond();

            return unix_epoch_ts;
            
        } catch ( Exception ex ){
            return 0;
        }
    }
    
}
